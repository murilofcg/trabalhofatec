Private Sub btnCalc_Click()
Dim somaH As Integer, i As Integer
Dim salM As Double, salV As Double, salN As Double, turno As String
Dim codMaiorSal As Integer, maiorSal As Double
Dim totSalGer As Double, totGer As Integer, mediaSalGer As Double
Dim linha As Integer, msg As String

linha = Range("a200").End(xlUp).Row
somaH = 0
salM = 0
salV = 0
salN = 0
maiorSal = 0
totGer = 0
totSalGer = 0

For i = 2 To linha
    somaH = somaH + Cells(i, 2).Value
    
    turno = Cells(i, 3).Value
    If turno = "Matutino" Then
        salM = salM + Cells(i, 8).Value
    ElseIf turno = "Vespertino" Then
        salV = salV + Cells(i, 8).Value
    Else
        salN = salN + Cells(i, 8).Value
    End If
    
    If Cells(i, 8).Value > maiorSal Then
        codMaiorSal = Cells(i, 1).Value
    End If
    
    If Cells(i, 4).Value = "Gerente" Then
        totGer = totGer + 1
        totSalGer = totSalGer + Cells(i, 8).Value
    End If
    
Next i

If totGer > 0 Then
    mediaSalGer = totSalGer / totGer
Else
    mediaSalGer = 0
End If

msg = "A soma das horas trabalhadas = " & somaH
msg = msg & vbCrLf & "O sal�rio Matutino = " & salM
msg = msg & vbCrLf & "O sal�rio Vespertino = " & salV
msg = msg & vbCrLf & "O sal�rio Noturno = " & salN
msg = msg & vbCrLf & "O c�digo do funcion�rio com maior sal�rio = " & codMaiorSal
msg = msg & vbCrLf & "O sal�rio m�dio dos gerentes = " & mediaSalGer
lblResult.Caption = msg

End Sub